import { NextApiRequest, NextApiResponse } from 'next';
import DummyTable from '../../models/DummyTable';

export default async (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method === 'PUT') {
        try {
            const { ComplaintArea, FPR_EMPNO, SPR_EMPNO, TPR_EMPNO } = req.body;
            const updated = await DummyTable.update(
                { FPR_EMPNO, SPR_EMPNO, TPR_EMPNO },
                { where: { ComplaintArea } }
            );
            if (updated[0] > 0) {
                res.status(200).json({ message: 'Entry updated successfully' });
            } else {
                res.status(404).json({ error: 'Complaint area not found' });
            }
        } catch (error) {
            res.status(500).json({ error: 'Failed to update data' });
        }
    } else {
        res.status(405).end();
    }
};