import { NextApiRequest, NextApiResponse } from 'next';
import DummyTable from '../../models/DummyTable';

export default  async(req: NextApiRequest, res: NextApiResponse) => {
    if (req.method === 'GET') {
        try {
            const { complaintArea } = req.query;
            if (complaintArea) {
                const entry = await DummyTable.findOne({ where: { ComplaintArea: complaintArea } });
                if (entry) {
                    res.status(200).json(entry);
                } else {
                    res.status(404).json({ error: 'Complaint area not found' });
                }
            } else {
                const complaintAreas = await DummyTable.findAll({ attributes: ['ComplaintArea'] });
                res.status(200).json(complaintAreas);
            }
        } catch (error) {
            res.status(500).json({ error: 'Failed to fetch data' });

        }
    } else {
        res.status(405).end();
    }
};

// export default dummy ;