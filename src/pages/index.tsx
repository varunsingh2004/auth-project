'use client'
import { useState, useEffect, ChangeEvent, FormEvent } from 'react';
import axios from 'axios';
import {
  Select,
  MenuItem,
  TextField,
  Button,
  Container,
  Typography,
  Box,
  CircularProgress,
  FormControl,
  InputLabel,
  SelectChangeEvent
} from '@mui/material';
import dummy from './api/dummy';

interface DummyTableAttributes {
  ComplaintArea: string;
  FPR_EMPNO: number;
  FPR_NAME: string;
  FPR_EMAIL: string;
  SPR_EMPNO: number;
  SPR_NAME: string;
  SPR_EMAIL: string;
  TPR_EMPNO: number;
  TPR_NAME: string;
  TPR_EMAIL: string;
}

const Home: React.FC = () => {
  const [complaintAreas, setComplaintAreas] = useState<string[]>([]);
  const [selectedComplaintArea, setSelectedComplaintArea] = useState<string>('');
  const [data, setData] = useState<DummyTableAttributes | null>(null);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    const fetchComplaintAreas = async () => {
      try {
        const response = await axios.get('/api/dummy');
        // const response = await dummy();
        setComplaintAreas(response.data.map((item: any) => item.ComplaintArea));
      } catch (error) {
        console.error('Error fetching complaint areas:', error);
        alert(error);
        return ;
        
      }
    };

    fetchComplaintAreas();
  }, []);

  const handleComplaintAreaChange = async (event: SelectChangeEvent<string>) => {
    setSelectedComplaintArea(event.target.value);
    setLoading(true);

    try {
      const response = await axios.get('/api/dummy', {
        params: { complaintArea: event.target.value }
      });
      setData(response.data);
    } catch (error) {
      console.error('Error fetching data:', error);
      alert(error);
      return ;
    }

    setLoading(false);
  };

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setData(prevData => prevData ? { ...prevData, [name]: value } : null);
  };

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (data) {
      try {
        await axios.put('/api/update', data);
        alert('Data updated successfully');
      } catch (error) {
        console.error('Error updating data:', error);
        alert(error);
        return ;
      }
    }
  };

  return (
    <Container maxWidth="sm">
      <Box component="form" onSubmit={handleSubmit} sx={{ mt: 4 }}>
        <Typography variant="h4" component="h1" gutterBottom>
          Dummy Table Form
        </Typography>

        <FormControl fullWidth sx={{ mb: 2 }}>
          <InputLabel id="complaint-area-label">Complaint Area</InputLabel>
          <Select
            labelId="complaint-area-label"
            value={selectedComplaintArea}
            onChange={handleComplaintAreaChange}
            label="Complaint Area"
          >
            <MenuItem value="" disabled>Select Complaint Area</MenuItem>
            {complaintAreas.map((area, index) => (
              <MenuItem key={index} value={area}>{area}</MenuItem>
            ))}
          </Select>
        </FormControl>

        {loading && <CircularProgress />}

        {data && (
          <>
            <TextField
              fullWidth
              label="FPR EMPNO"
              type="number"
              name="FPR_EMPNO"
              value={data.FPR_EMPNO}
              onChange={handleInputChange}
              sx={{ mb: 2 }}
            />
            <TextField
              fullWidth
              label="SPR EMPNO"
              type="number"
              name="SPR_EMPNO"
              value={data.SPR_EMPNO}
              onChange={handleInputChange}
              sx={{ mb: 2 }}
            />
            <TextField
              fullWidth
              label="TPR EMPNO"
              type="number"
              name="TPR_EMPNO"
              value={data.TPR_EMPNO}
              onChange={handleInputChange}
              sx={{ mb: 2 }}
            />
            <Button type="submit" variant="contained" color="primary" fullWidth>
              Update
            </Button>
          </>
        )}
      </Box>
    </Container>
  );
};

export default Home;