import { DataTypes, Model, Optional } from 'sequelize';
import sequelize from '../lib/sequelize';

interface DummyTableAttributes {
    ComplaintArea: string;
    FPR_EMPNO: number;
    FPR_NAME: string;
    FPR_EMAIL: string;
    SPR_EMPNO: number;
    SPR_NAME: string;
    SPR_EMAIL: string;
    TPR_EMPNO: number;
    TPR_NAME: string;
    TPR_EMAIL: string;
}

class DummyTable extends Model<DummyTableAttributes> implements DummyTableAttributes {
    public ComplaintArea!: string;
    public FPR_EMPNO!: number;
    public FPR_NAME!: string;
    public FPR_EMAIL!: string;
    public SPR_EMPNO!: number;
    public SPR_NAME!: string;
    public SPR_EMAIL!: string;
    public TPR_EMPNO!: number;
    public TPR_NAME!: string;
    public TPR_EMAIL!: string;
}

DummyTable.init({
    ComplaintArea: {
        type: DataTypes.STRING(50),
        primaryKey: true
    },
    FPR_EMPNO: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    FPR_NAME: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
    FPR_EMAIL: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
    SPR_EMPNO: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    SPR_NAME: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
    SPR_EMAIL: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
    TPR_EMPNO: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    TPR_NAME: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
    TPR_EMAIL: {
        type: DataTypes.STRING(100),
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'DummyTable',
    timestamps: false
});

export default DummyTable;